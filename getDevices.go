package main

import (
	"bufio"
	"crypto/tls"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/jwalton/go-supportscolor"
	"golang.org/x/net/icmp"
	"golang.org/x/net/ipv4"
)

func main() {
	now := time.Now()

	var pass int
	var fail int
	var tout time.Duration = 5

	if len(os.Args) < 2 {
		er(fmt.Errorf("must specify at least one argument"))
	}

	args := os.Args[1:]

	for n := 0; n < len(args); n++ {
		arg := args[n]

		exe, err := os.Executable()
		er(err)

		if arg == "--help" || arg == "-h" {
			fmt.Print("-h | --help\tPrint this help message\n")
			fmt.Printf("Usage: %s <CONF FILE 1> <CONF FILE 2> ...\n", filepath.Base(exe))
			fmt.Print("\nConfig File Format:\n")
			fmt.Print("<FQDN|IP>,<PORT>,<CHECK>:<INT>\n")
			fmt.Print("Supported Checks:\n")
			fmt.Print("  SSL:\n")
			fmt.Print("    <FQDN|IP>,<PORT>,SSL:<INT>   Where <INT> is the number of days of validity before check is marked as a failure\n")
			fmt.Print("  ICMP:\n")
			fmt.Print("    <FQDN|IP>,N/A,ICMP:<INT>     Where <INT> is the number of milliseconds of response time before check is marked as a failure\n")
			fmt.Print("  HTTP:\n")
			fmt.Print("    <FQDN|IP>,<PORT>,HTTP:<INT>  Where <INT> is the expected response code of the HTTP server\n")
			fmt.Print("  HTTPS:\n")
			fmt.Print("    <FQDN|IP>,<PORT>,HTTPS:<INT> Where <INT> is the expected response code of the HTTPS server\n")
			return
		}

		var Reset = "\033[0m"
		var Red = "\033[31m"
		var Green = "\033[32m"

		if !supportscolor.Stdout().SupportsColor {
			Reset = ""
			Red = ""
			Green = ""
		}

		file := arg

		_, err = os.Stat(file)
		er(err)

		fh, err := os.Open(file)
		er(err)
		defer fh.Close()

		scanner := bufio.NewScanner(fh)
		for scanner.Scan() {

			regex := regexp.MustCompile(`^#`)

			if regex.MatchString(scanner.Text()) {
				continue
			}

			regex = regexp.MustCompile(`^([^,]+),([^,]*),([^,:]+):?([^, ]+)?\s*#*.*$`)

			if regex.MatchString(scanner.Text()) {
				host := strings.TrimSpace(regex.FindStringSubmatch(scanner.Text())[1])
				port := strings.TrimSpace(regex.FindStringSubmatch(scanner.Text())[2])
				check := strings.TrimSpace(regex.FindStringSubmatch(scanner.Text())[3])
				opt := strings.TrimSpace(regex.FindStringSubmatch(scanner.Text())[4])
				switch check {
				case "SSL":
					fmt.Print("SSL Check:   ")
					days, err := strconv.Atoi(opt)
					er(err)

					tls_conf := &tls.Config{
						InsecureSkipVerify: true,
					}

					conn, err := tls.DialWithDialer(&net.Dialer{Timeout: tout * time.Second}, "tcp", fmt.Sprintf("%s:%s", host, port), tls_conf)
					if err != nil {
						fmt.Printf("%sFAIL:%s %s: Can not connect\n", Red, Reset, host)
						continue
					}
					defer conn.Close()

					certs := conn.ConnectionState().PeerCertificates

					expire := certs[0].NotAfter
					time_limit := now.AddDate(0, 0, days)

					if expire.Before(time_limit) {
						fmt.Printf("%sFAIL:%s %s: Certificate expires in less than %d days (%s)\n", Red, Reset, host, days, expire.Format("2006.01.02 15:04:05"))
						fail = fail + 1
					} else {
						fmt.Printf("%sPASS:%s %s: Certificate expires %s\n", Green, Reset, host, expire.Format("2006.01.02 15:04:05"))
						pass = pass + 1
					}

				case "HTTP":
					fmt.Print("HTTP Check:  ")
					res, err := http.Get(fmt.Sprintf("http://%s:%s", host, port))
					if err != nil {
						fmt.Printf("%sFAIL:%s %s: Can not connect\n", Red, Reset, host)
						continue
					}
					rc := res.StatusCode
					expected_code, err := strconv.Atoi(opt)
					er(err)

					if rc != expected_code {
						fmt.Printf("%sFAIL:%s %s: Returned response code: %s (!= %d)\n", Red, Reset, host, res.Status, expected_code)
					} else {
						fmt.Printf("%sPASS:%s %s: Returned response code: %s\n", Green, Reset, host, res.Status)
					}

				case "HTTPS":
					fmt.Print("HTTPS Check: ")
					tr := &http.Transport{
						TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
					}
					client := &http.Client{Transport: tr}
					res, err := client.Get(fmt.Sprintf("https://%s:%s", host, port))
					if err != nil {
						fmt.Printf("%sFAIL:%s %s: Can not connect\n", Red, Reset, host)
						continue
					}

					rc := res.StatusCode

					expected_code, err := strconv.Atoi(opt)
					er(err)

					if rc != expected_code {
						fmt.Printf("%sFAIL:%s %s: Returned response code: %s (!= %d)\n", Red, Reset, host, res.Status, expected_code)
					} else {
						fmt.Printf("%sPASS:%s %s: Returned response code: %s\n", Green, Reset, host, res.Status)
					}

				case "ICMP":
					fmt.Print("ICMP Check:  ")
					localAddr := "0.0.0.0"
					latency_limit, err := strconv.Atoi(opt)
					er(err)

					conn, err := icmp.ListenPacket("ip4:icmp", localAddr)
					if err != nil {
						fmt.Printf("%sFAIL:%s %s: Can not connect\n", Red, Reset, host)
						continue
					}
					defer conn.Close()

					exe, err := os.Executable()
					er(err)

					msg := icmp.Message{
						Type: ipv4.ICMPTypeEcho,
						Code: 0,
						Body: &icmp.Echo{
							ID:   os.Getegid() & 0xffff,
							Seq:  42,
							Data: []byte(filepath.Base(exe)),
						},
					}

					wb, err := msg.Marshal(nil)
					er(err)
					rb := make([]byte, 1500)

					dest, err := net.ResolveIPAddr("ip", host)
					if err != nil {
						fmt.Printf("%sFAIL:%s %s: Can not connect\n", Red, Reset, host)
						continue
					}

					err = conn.SetDeadline(time.Now().Add(tout * time.Second))
					if err != nil {
						fmt.Printf("%sERROR%s (%s) %v\n", Red, Reset, host, err)
						continue
					}

					start := time.Now()
					_, err = conn.WriteTo(wb, dest)
					if err != nil {
						fmt.Printf("%sERROR%s (%s) %v\n", Red, Reset, host, err)
						continue
					}

					n, _, err := conn.ReadFrom(rb)
					if err != nil {
						fmt.Printf("%sERROR%s (%s) %v\n", Red, Reset, host, err)
						continue
					}

					duration := int(time.Since(start).Milliseconds())

					rm, err := icmp.ParseMessage(1, rb[:n])
					er(err)

					switch rm.Type {
					case ipv4.ICMPTypeEchoReply:
						if duration < latency_limit {
							fmt.Printf("%sPASS:%s %s: Response time %dms (<%dms)\n", Green, Reset, host, duration, latency_limit)
						} else {
							fmt.Printf("%sFAIL:%s %s: Response time %dms (>%dms)\n", Red, Reset, host, duration, latency_limit)
						}
					default:
						fmt.Printf("%sFAIL:%s %s: No response (%+v)\n", Red, Reset, host, rm)
					}
				}
			}
		}
	}
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(),
			path.Base(file), line, err)
	}
}
