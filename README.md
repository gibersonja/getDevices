# Usage
```bash
./getDevices config1 config2 ...
```

# Config File Format

## SSL Checks
```csv
<FQDN|IP>,<PORT>,SSL:<INT>
```
Where <INT> is the number of days of validity before check is marked as a failure

## ICMP Checks
```csv
<FQDN|IP>,,ICMP:<INT>
```
Where <INT> is the number of milliseconds of response time before the check is marked as a failure
The <PORT> field is ignored for ICMP Checks

## HTTP
```csv
<FQDN|IP>,<PORT>,HTTP:<INT>
```
Where <INT> is the expected response code of the HTTP server

## HTTPS
```csv
<FQDN|IP>,<PORT>,HTTPS:<INT>
```
Where <INT> is the expected response code of the HTTPS server

# Config File Example

See `config.cfg`

```csv
#<FQDN|IP>, <PORT>, <TYPE:INT>

neverssl.com,80,HTTP:500
google.com,80,HTTP:200
google.com,443,HTTPS:200
google.com,443,HTTPS:500
google.com,443,SSL:300
google.com,443,SSL:3
google.com,,ICMP:2
google.com,,ICMP:200

afasfdasdf,43,HTTPS:200
afasfdasdf,43,HTTP:200
asfasfd,,ICMP:400
asfsadf,23,SSL:30
```


