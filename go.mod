module getDevices

go 1.18

require (
	golang.org/x/net v0.18.0
	golang.org/x/term v0.14.0
)

require (
	github.com/jwalton/go-supportscolor v1.2.0 // indirect
	golang.org/x/sys v0.15.0 // indirect
)
